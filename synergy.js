<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.min.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/vendors/jquery.easings.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.extensions.min.js"></script>
<script>
$(document).ready(function() {
	var sections = $("#rec244635809, #rec244235381, #rec244236651, #rec244237549, #rec244238247, #rec244686780, #rec244723253, #rec226095562");

	sections.wrap("<div class='section'></div>");
	$(".section").wrapAll("<div id='fullpage'></div>");
	
	window.page = $('#fullpage').fullpage({
		keyboardScrolling: true,
		scrollingSpeed: 800, 
		
		css3: true,
		scrollBar: true,
	});
});
</script>
<style>
	body {
		overflow: hidden !important;
	}
</style>